<?php

interface Evaluable
{
    public function getPrice();
}

interface Measurable
{
    public function measureArea();
}

interface Marketable extends Evaluable, Measurable
{
    public function getStatus();
}

/**
 * superclass  Product
 */
abstract class Product implements Marketable
{
    private $status;
    private $price;
    private $maxDiscount;
    private $sizeLength;
    private $sizeWidth;
    private $sizeHeight;
    private $deliveryDay;

    private $storageSize = 10000;
    CONST DISCOUNT_PERIOD = 180;

    public function __construct($price, $maxDiscount, $deliveryDay)
    {
        $this->price = $price;
        $this->maxDiscount = $maxDiscount > 1 ? $maxDiscount / 100 : $maxDiscount;
        $this->deliveryDay = $deliveryDay;
    }

    public function setLength($length)
    {
        $this->sizeLength = $length;
        return $this;
    }

    public function setWidth($width)
    {
        $this->sizeWidth = $width;
        return $this;
    }

    public function setHeight($height)
    {
        $this->sizeHeight = $height;
        return $this;
    }

    public function getPrice()
    {
        $today = new DateTime();
        $interval = min($today->diff($this->deliveryDay)->d, self::DISCOUNT_PERIOD);
        $discount = $interval / self::DISCOUNT_PERIOD * $this->maxDiscount;
        return $this->price - $this->price * $discount;
    }

    public function measureArea()
    {
        if (isset($this->sizeLength, $this->sizeWidth, $this->sizeHeight, $this->storageSize)) {
            $area = $this->sizeLength * $this->sizeWidth * $this->sizeHeight;
            if ($area > $this->storageSize) {
                $this->status = 'Transportation by sea';
            }
            $this->status = 'Transportation by air';
            return $area;
        }
        return 0;
    }

    public function getStatus()
    {
        $today = new DateTime();
        $daysLeft = $today->diff($this->deliveryDay)->d;
        return "Days left $daysLeft. $this->status";
    }
}

interface  Describe
{
    public function getDescription();
}

/**
 * Class Car
 *
 */
class Car extends Product implements Describe
{
    private $model;
    private $bodyType;
    private $engineCapacity;
    private $musicSystem;
    private $navigation;
    private $options;

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function setBodyType($bodyType)
    {
        $this->bodyType = $bodyType;
        return $this;
    }

    public function setOptions($options)
    {
        $this->options = $options;
        switch ($options) {
            case 'base':
                $this->engineCapacity = 1.4;
                $this->musicSystem = 'none';
                $this->navigation = false;
                break;
            case 'comfort':
                $this->engineCapacity = 1.8;
                $this->musicSystem = 'sony';
                $this->navigation = false;
                break;
            case 'elegance':
                $this->engineCapacity = 3;
                $this->musicSystem = 'harman kardon';
                $this->navigation = true;
                break;
        }
        return $this;
    }

    public function getDescription() {
        $description = "Model: $this->model. Body type: $this->bodyType." . PHP_EOL .
                       "Equipment level:$this->options 
                        Engine capacity $this->engineCapacity
                        Price: " . $this->getPrice() . PHP_EOL;
        if($this->musicSystem !== 'none') {
            $description .= "Music by $this->musicSystem.";
        }

        return $description;
    }
}

/**
 * Class tv
 */
class Tv extends Product
{
    private $volume = 30;
    private $channel = 1;
    private $power = false;

    public function increaseVolume()
    {
        if ($this->getStatus()) {
            if ($this->volume < 100) {
                $this->volume++;
            }
        }
    }

    public function decreaseVolume()
    {
        if ($this->getStatus()) {
            if ($this->volume > 0) {
                $this->volume--;
            }
        }
    }

    public function setChanel($channel)
    {
        if ($this->getStatus()) {
            $this->channel = $channel;
        }
    }

    public function switchPower()
    {
        $this->power = !$this->power;
    }

    public function getStatus()
    {
        return $this->power;
    }

}

interface Writable {
    public function startWrite();
    public function endWrite();
}

class BallPen extends Product implements Writable
{
    const HOURS_OF_CONSUMPTION_IN_INK = 10;
    private $incLevel = 100;
    private $startMeasurement;

    public function startWrite()
    {
        $this->startMeasurement = new DateTime();
    }

    public function endWrite()
    {
        $endMeasurement = new DateTime();
        $hours = $endMeasurement->diff($this->startMeasurement)->days;
        if ($hours > 0) {
            $this->incLevel -= self::HOURS_OF_CONSUMPTION_IN_INK / $hours;
        }
    }
}

interface Quacking
{
    public function quackQuack();
}

final class Duck extends Product implements Quacking
{
    public $color;
    public $size;

    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    public function quackQuack()
    {
        echo "I`m a Duck! My color is $this->color.";
    }

}

$bmw = new Car (45000, 10, new DateTime('2017-01-02'));
$bmw->setModel('BMW');
$bmw->setOptions('elegance');
$bmw->setModel('BMW 320');
echo $bmw->getDescription();


$LG = new Tv(5000, 10, new DateTime('2018-01-01'));
$LG->switchPower();
$LG->increaseVolume();
$LG->getStatus();

$Sony = new Tv(8000, 10, new DateTime('2018-02-01'));
$Sony->increaseVolume();
$Sony->switchPower();

$pen = new BallPen(100, 0, new DateTime('2018-01-01'));
$pen->startWrite();
$pen->endWrite();

$duckyDuck = new Duck(1000, 5, new DateTime('2018-01-01'));
$duckyDuck->setColor('Yellow');



?>
<pre>
<?php
?>




