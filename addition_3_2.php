<?php

trait PriceDependenceOfWeight
{
    public function getPrice()
    {
        $discount = $this->price * min(max($this->discountPercent, 0), 100) / 100;
        $price = $this->price - $discount;
        return "Price    is :" . $price . '<br>' .
               "Discount is :" . $discount . '<br>';
    }

}


abstract class Product
{
    use PriceDependenceOfWeight;

    public $price;
    public $weight;
    public $discountPercent = 10;
    CONST DELIVERY_WITH_DISCOUNT = 250;
    CONST DELIVERY_WITHOUT_DISCOUNT = 300;

    public function __construct($price, $weight)
    {
        $this->price = $price;
        $this->weight = $weight;
    }

    public function getDeliveryPrice()
    {
        $price = $this->discountPercent > 0 ? self::DELIVERY_WITH_DISCOUNT : self::DELIVERY_WITHOUT_DISCOUNT;
        return "Delivery price is :" . $price . '<br>';
    }
}

class Monitor extends Product
{
    public function getPrice()
    {
        if ($this->weight < 10) {
            $this->discountPercent = 0;
        }
        return parent::getPrice();
    }
}

class Notebook extends Product
{

}

class Printer extends Product
{

}

echo '<br>' . '<br>' ;

$lg = new Monitor(100, 5);
echo $lg->getPrice();
echo $lg->getDeliveryPrice();

echo '<br>' . '<br>' ;

$sony = new Monitor(100, 10);
echo $sony->getPrice();
echo $sony->getDeliveryPrice();

echo '<br>' . '<br>' ;

$dell = new Monitor(100, 15);
echo $dell->getPrice();
echo $dell->getDeliveryPrice();

echo '<br>' . '<br>' . 'Notebook' . '<br>' . '<br>' ;

$lenovo = new Notebook(100, 5);
echo $lenovo->getPrice();
echo $lenovo->getDeliveryPrice();

echo '<br>' . '<br>' ;

$apple = new Notebook(100, 10);
echo $apple->getPrice();
echo $apple->getDeliveryPrice();

echo '<br>' . '<br>' ;

$asus = new Notebook(100, 15);
echo $asus->getPrice();
echo $asus->getDeliveryPrice();
