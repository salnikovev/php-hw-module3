<?php

/**
 * Class Car
 *
 */
class Car
{
    private $model;
    private $bodyType;
    private $options;

    private $engineCapacity;
    private $musicSystem;
    private $navigation;
    private $startPrice;
    private $deliveryDay;
    private $maxDiscount;


    public function __construct($model, $bodyType, $options, $startPrice)
    {
        $this->model = $model;
        $this->bodyType = $bodyType;
        $this->options = $options;
        $this->startPrice = $startPrice;
        $this->deliveryDay = new DateTime();

        switch ($options) {
            case 'base':
                $this->engineCapacity = 1.4;
                $this->musicSystem = 'none';
                $this->navigation = false;
                break;
            case 'comfort':
                $this->engineCapacity = 1.8;
                $this->musicSystem = 'sony';
                $this->navigation = false;
                break;
            case 'elegance':
                $this->engineCapacity = 3;
                $this->musicSystem = 'harman kardon';
                $this->navigation = true;
                break;
        }
    }

    public function getPrice()
    {
        $today = new DateTime();
        $interval = min($today->diff($this->deliveryDay)->d, 180);
        $discount = $interval / 180 * $this->maxDiscount;
        return $this->startPrice - $this->startPrice * $discount;
    }

    public function setDiscount($discount)
    {
        if ($discount > 70) {
            $discount = 70;
        }

        $this->maxDiscount = $discount / 100;
    }

}

/**
 * Class tv
 */
class Tv
{
    private $volume = 30;
    private $channel = 1;
    private $power = false;

    public function increaseVolume()
    {
        if ($this->getStatus()) {
            if ($this->volume < 100) {
                $this->volume++;
            }
        }
    }

    public function decreaseVolume()
    {
        if ($this->getStatus()) {
            if ($this->volume > 0) {
                $this->volume--;
            }
        }
    }

    public function setChanel($channel)
    {
        if ($this->getStatus()) {
            $this->channel = $channel;
        }
    }

    public function switchPower()
    {
        $this->power = !$this->power;
    }

    public function getStatus()
    {
        return $this->power;
    }

}

class BallPen
{
    const HOURS_OF_CONSUMPTION_IN_INK = 10;
    private $incLevel = 100;
    private $startMeasurement;

    public function startWrite()
    {
        $this->startMeasurement = new DateTime();
    }

    public function endWrite()
    {
        $endMeasurement = new DateTime();
        // для проверки меняем на секунды
        $hours = $endMeasurement->diff($this->startMeasurement)->s;
        $this->incLevel -= self::HOURS_OF_CONSUMPTION_IN_INK / $hours;

    }
}

class Duck
{
    public $color;
    public $size;

    public function __construct($color, $size)
    {
        $this->color = $color;
        $this->size = $size;

    }

    public function quackQuack()
    {
        echo "I`m a Duck! My color is $this->color. I`m very $this->size";
    }

}

class Product
{
    private $status;
    private $daysLeft;
    private $daysToThePoint = 10;
    private $storageSize = 10000;
    private $sizeLength;
    private $sizeWidth;
    private $sizeHeight;


    public function __construct($length, $width, $height)
    {
        $this->daysLeft = Date();
        $this->sizeLength = $length;
        $this->sizeWidth = $width;
        $this->sizeHeight = $height;
        $this->calculate();
    }

    public function calculate()
    {
        $area = $this->sizeLength * $this->sizeWidth * $this->sizeHeight;
        if ($area > $this->storageSize) {
            $this->status = 'Transportation by sea';
            $this->transport();
        }

        $this->status = 'Transportation by air';
    }

    public function transport()
    {
        $this->daysToThePoint += 100;
    }

    public function getStatus()
    {
        return "Days left $this->daysLeft. Days to the point of issue $this->daysToThePoint. $this->status";
    }

}


$bmw = new Car ('BMW', 'coupe', 'elegance', 45000);
$bmw->setDiscount(500);

$ford = new Car ('Ford', 'hatchback', 'base', 20000);
$ford->setDiscount(30);

$LG = new Tv();
$LG->switchPower();
$LG->increaseVolume();
$LG->getStatus();

$Sony = new Tv();
$Sony->increaseVolume();
$Sony->switchPower();

$pen = new BallPen();
$pen->startWrite();
sleep(10);
$pen->endWrite();

$duckyDuck = new Duck('Yellow', 'small');
$duckyDuck->quackQuack();

$bigBoxesOfGoods = new Product(100, 200, 450);
$bigBoxesOfGoods->getStatus();

?>
    <pre>
<?php
var_dump($bmw);
var_dump($ford);
var_dump($LG);
var_dump($Sony);

var_dump($pen);
var_dump($duckyDuck);
var_dump($bigBoxesOfGoods);
?>




